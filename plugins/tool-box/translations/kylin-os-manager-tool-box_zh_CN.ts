<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation>敬请期待</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation>更多系统小工具即将上线</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation>百宝箱</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="64"/>
        <source>My Tool</source>
        <translation>我的工具</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="68"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>各类系统小工具，帮您更好使用电脑</translation>
    </message>
</context>
</TS>
