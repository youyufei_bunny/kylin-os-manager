<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="106"/>
        <location filename="../cleandetailveiw.cpp" line="111"/>
        <location filename="../cleandetailveiw.cpp" line="288"/>
        <source>Computer scan in progress...</source>
        <translation>ཡར་ཐོན་གྱི་རྩིས་འཁོར་ལ་ཞིབ་བཤེར་བྱེད་བཞིན་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="114"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Cleanup</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="130"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="137"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="177"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>System cache</source>
        <translation>མ་ལག་གི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="185"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>ཁ་གསལ་བའི་ཐུམ་སྒྲིལ་དང་། མཐེབ་བཀྱག་དང་བལྟ་ཀློག་ཆས་ཀྱི་མགྱོགས་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="236"/>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="220"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Cookies</source>
        <translation>བག་ལེབ་ཀོར་མོ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="862"/>
        <source>Clearance completed</source>
        <translation>གཙང་བཤེར་བྱས་ཚར་སོང་།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="863"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>རྩིས་འཁོར་ལ་གསོན་ཤུགས་ཧ་ཅང་ཆེན་པོ་ཡོད་པས་གཙང་སྦྲའི་གོམས་གཤིས་རྒྱུན་འཁྱོངས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="259"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Historical trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="341"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <source>Computer cleanup in progress...</source>
        <translation>རྩིས་འཁོར་གཙང་བཤེར་བྱེད་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="465"/>
        <location filename="../cleandetailveiw.cpp" line="467"/>
        <source>Cleanable cache </source>
        <translation>གཙང་སྦྲ་དོད་པའི་མགྱོགས་ཚད། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source> items</source>
        <translation> རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="534"/>
        <source> historical use traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་བེད་སྤྱོད་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <location filename="../cleandetailveiw.cpp" line="644"/>
        <source>Cleanable Cache</source>
        <translation>གཙང་སྦྲ་དོད་པའི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <location filename="../cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cookie</source>
        <translation>གཙང་སྦྲ་དོད་པའི་ཀ་ར་གོ་རེ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="700"/>
        <location filename="../cleandetailveiw.cpp" line="702"/>
        <source>Clear cache </source>
        <translation>ཁ་གསལ་གཏིང་གསལ་ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source>Clear cookie </source>
        <translation>བག་ལེབ་ཀོར་མོ་གཙང་མ། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="228"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>དྲ་སྦྲེལ་དང་། རོལ་རྩེད། ཟོག་ཉོའི་ལོ་རྒྱུས་སོགས་ཁ་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Clear system usage traces</source>
        <translation>མ་ལག་བཀོལ་སྤྱོད་ཀྱི་རྗེས་ཤུལ་ཁ་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <source>Cleanable browser </source>
        <translation>གཙང་སྦྲ་དོད་པའི་བལྟ་ཆས། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>system cache</source>
        <translation>མ་ལག་གི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>cookie record</source>
        <translation>བག་ལེབ་ཀོར་མོའི་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="588"/>
        <source>history trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item,</source>
        <translation> རྣམ་གྲངས་དང་།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="600"/>
        <location filename="../cleandetailveiw.cpp" line="615"/>
        <source> item</source>
        <translation> རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="603"/>
        <source>Complete</source>
        <translation>ཆ་ཚང་བ་ཞིག་རེད་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source>Clear </source>
        <translation>གསལ་པོར་བཤད་ན། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source> historical traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="837"/>
        <location filename="../cleandetailveiw.cpp" line="840"/>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <location filename="../cleandetailveiw.cpp" line="971"/>
        <location filename="../cleandetailveiw.cpp" line="972"/>
        <location filename="../cleandetailveiw.cpp" line="973"/>
        <source>Cleaning up......</source>
        <translation>གཙང་སྦྲ་བྱེད་པ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="956"/>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <source>Cleaning up</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="961"/>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <source>Cleaning up..</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="966"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <source>Cleaning up....</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="73"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>རྩིས་འཁོར་གྱི་གད་སྙིགས་དང་ལྡེ་མིག་གཅིག་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>དུས་བཀག་ལྟར་གཙང་སྦྲ་བྱས་ཏེ་གློག་ཀླད་ཀྱི་འོད་དང་བདེ་འཇགས་ལ་སྲུང་སྐྱོབ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <location filename="../clearmainwidget.cpp" line="297"/>
        <source>System cache</source>
        <translation>མ་ལག་གི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <location filename="../clearmainwidget.cpp" line="312"/>
        <source>Cookies</source>
        <translation>བག་ལེབ་ཀོར་མོ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="128"/>
        <location filename="../clearmainwidget.cpp" line="327"/>
        <source>History trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="160"/>
        <source>StartClear</source>
        <translation>འགོ་རྩོམ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <source>Cleanup Package Cache</source>
        <translation>གཙང་བཤེར་བྱེད་པའི་ཁུག་མའི་ནང་དུ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>གཙང་བཤེར་བྱས་པའི་མཐེ་བོང་བསྒྲེངས་པའི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="253"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <location filename="../clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>ཨན་ཞིན་བཤར་ཆས་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="254"/>
        <source>Cleanup Trash Box</source>
        <translation>གད་སྙིགས་བླུགས་སྒམ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="379"/>
        <location filename="../clearmainwidget.cpp" line="392"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>ཆི་ཨན་ཞིན་བཤར་ཆས་།ནང་དུ་གྲོན་ཆུང་བྱས་པའི་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="350"/>
        <location filename="../clearmainwidget.cpp" line="363"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>མེ་སྐྱོན་ཁྲོད་གྲོན་ཆུང་བྱས་པའི་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="268"/>
        <source>Clean up the recently opened documents records</source>
        <translation>ཉེ་ཆར་ཁ་ཕྱེས་པའི་ཡིག་ཆའི་ཟིན་ཐོ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="269"/>
        <source>Delete the command history</source>
        <translation>བཀོད་འདོམས་ཀྱི་ལོ་རྒྱུས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <location filename="../clearmainwidget.cpp" line="359"/>
        <source>Cleanup FireFox Cache</source>
        <translation>ཝ་དམར་བཤར་ཆས་གཙང་བཤེར་ལྷོད་གསོག་</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <location filename="../clearmainwidget.cpp" line="417"/>
        <source>Cleanup Chromium Cache</source>
        <translation>ཀོའུ་ཀོའུ་བཤར་ཆས་གཙང་བཤེར་དལ་གསོག་།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <location filename="../clearmainwidget.cpp" line="421"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>ཀོའུ་ཀོ་བཤར་ཆས་Cookiesགཙང་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="412"/>
        <location filename="../clearmainwidget.cpp" line="425"/>
        <source>Clean up the Chromium Internet records</source>
        <translation>ཀོའུ་ཀོའུ་བཤར་ཆས་ནང་གི་ཟིན་ཐོ་གཙང་བཤེར་།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="109"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>ཐུམ་སྒྲིལ་དང་མཐེ་བོང་གཙང་བཤེར་བྱས་ནས་གད་སྙིགས་བླུགས་སྒམ་དུ་བསྐྱར་དུ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="119"/>
        <source>Clean up Internet history,cookies</source>
        <translation>དྲ་རྒྱའི་ལོ་རྒྱུས་དང་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="129"/>
        <source>Clean up system usage traces</source>
        <translation>མ་ལག་གི་བཀོལ་སྤྱོད་རྗེས་ཤུལ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>གཙང་སྦྲ་དོད་པའི་རྣམ་གྲངས་བདམས་ཐོན་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>གཙང་སྦྲའི་རྣམ་གྲངས་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས་པའི་དངོས་རྫས་མེད་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation>གད་སྙིགས་གཙང་བཤེར།</translation>
    </message>
</context>
</TS>
