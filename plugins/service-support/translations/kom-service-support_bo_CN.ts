<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>ཞིབ་ཕྲའི་རིགས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>མ་ལག་གི་འགུལ་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>མ་ལག་སྒྲིག་སྦྱོར་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>མ་ལག་ཐོར་ཞིག་ཏུ་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>མ་ལག་གི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>མ་ལག་སྒྲིག་གཞི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>ལམ་ལུགས་ཀྱི་རྨང་གཞིའི་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།དཔེར་ན་།ཁ་འབྱེད་མ་ཐུབ་།མ་ལག་དང་།འབྲེལ་ཡོད་བཀོད་སྒྲིག་དང་།མ་ལག་གི་ནུས་པ་མི་གསལ་བ་སོགས་རེད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།དཔེར་ན་།ཕྱིའི་སྦྲེལ་མཐུད་ཕམ་དང་།མཉམ་སྤྱོད་བྱེད་ལས་སྒྲིག་འགོད།ཕྱི་བཀོད་འཚམ་སྦྱོར་སོགས་བྱེད་ཀྱི་ཡོད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཕྲ་བརྗོད་དང་།དཔེར་ན་ཐོབ་ལེན་དང་།སྒྲིག་སྦྱོར་དང་བཤིག་འདོན་ཆི་ལིན་མཉེན་ཆས་ནོར་འཁྲུལ་རེད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>རོགས་།ཁྱེད་རང་གི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།ཡང་ན་ཁྱེད་རང་གི་འདིར་བྲིས་ཀྱང་ཆོག་ཁྱེད་རང་གི་རེ་བའམ་ཡང་ན་དཔྱད་གཏམ་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>མཐའ་སྐོར།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>མཐའ་འཁོར་གྱི་མཐུན་འཕྲོད་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>མཐའ་སྐོར་ཁ་ལོ་བའི་ཉོ་སྒྲུབ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>མཐའ་འཁོར་གྱི་བཀོལ་སྤྱོད་དང་ནོར་འཁྲུལ་གྱི་སྙན་ཞུ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>རེ་འདུན་ཞུ་ཡིག</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>མཉེན་ཆས་བཀོལ་སྤྱོད་དང་ནོར་འཁྲུལ་གྱི་སྙན་ཞུ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>བསམ་འཆར་དང་གྲོས་འགོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="149"/>
        <source>Attachment size exceeds limit!</source>
        <translation>ཟུར་གཏོགས་ཀྱི་གཞི་ཁྱོན་ཚད་ལས་བརྒལ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="61"/>
        <source>Failed to create temporary directory!</source>
        <translation>གནས་སྐབས་ཀྱི་དཀར་ཆག་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>ཁྱོན་བསྡོམས</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="191"/>
        <source>System log</source>
        <translation>མ་ལག་གི་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="192"/>
        <source>Machine</source>
        <translation>འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="193"/>
        <source>Hardware</source>
        <translation>མཁྲེགས་ཆས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="194"/>
        <source>Drive</source>
        <translation>སྒུལ་ཤུགས་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="195"/>
        <source>APP list</source>
        <translation>APPཡི་མིང་ཐོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="196"/>
        <source>Rules</source>
        <translation>སྒྲིག་སྲོལ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="197"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="198"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ཞབས་ཞུའི་ལས་རིགས་ཀྱི་རྒྱབ་ཕྱོགས་མཁོ་འདོན</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="81"/>
        <source>ServiceSupport</source>
        <translation>ཞབས་ཞུའི་ལས་རིགས་ཀྱི་རྒྱབ་ཕྱོགས་མཁོ་འདོན</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="84"/>
        <source>Multi-channel technical support services</source>
        <translation>ཐབས་ལམ་མང་པོར་བརྟེན་ནས་ལག་རྩལ་རོགས་སྐྱོར་བྱེད་པའི་ཞབས་ཞུ་</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="87"/>
        <source>Feedback</source>
        <translation>ལྡོག་འདྲེན་བསམ་འཆར།</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="90"/>
        <source>Self service</source>
        <translation>རང་གིས་རང་ལ་ཞབས་འདེགས</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="103"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="110"/>
        <source> to get more services</source>
        <translation> དེ་བས་མང་བ་ཐོབ་།</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Creation time</source>
        <translation>གསར་རྩོམ་བྱེད་པའི་དུས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="71"/>
        <source>bydesign</source>
        <translation>ཇུས་འགོད་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="73"/>
        <source>duplicate</source>
        <translation>འདྲ་བཟོ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="75"/>
        <source>external</source>
        <translation>ཕྱི་རོལ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="77"/>
        <source>fixed</source>
        <translation>གཏན་འཇགས་ཅན།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="79"/>
        <source>notrepro</source>
        <translation>ནོར་ཆུ་ཕུ་རོ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="81"/>
        <source>postponed</source>
        <translation>དུས་འགྱངས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="83"/>
        <source>willnotfix</source>
        <translation>ཐག་མ་ཆོད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="99"/>
        <source>verify</source>
        <translation>ར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="102"/>
        <source>Has the issue been resolved?</source>
        <translation>གནད་དོན་ཐག་བཅད་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="105"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>གལ་ཏེ་ཐག་གཅོད་བྱས་ཚེ།གནད་དོན་དེ་སྒོ་རྒྱག་དགོས་པས་སྔར་ལས་ལྷག་པའི་བྱ་སྤྱོད་སྤེལ་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="107"/>
        <source>resolved</source>
        <translation>ཐག་གཅོད།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="109"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Description</source>
        <translation>གསལ་བཤད་ཡི་གེ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Progress</source>
        <translation>ཡར་ཐོན།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="194"/>
        <source>No record</source>
        <translation>ཟིན་ཐོ་མེད་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="195"/>
        <source>There is a network problem, please try again later</source>
        <translation>དྲ་རྒྱའི་གནད་དོན་ཡོད་པས་རྗེས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="196"/>
        <source>Loading, please wait</source>
        <translation>དངོས་ཟོག་བླུགས་ནས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="209"/>
        <source>retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="109"/>
        <source>Advanced</source>
        <translation>བཀྲམ་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="114"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>རོགས་།ཞིབ་ཕྲའི་ངང་གནད་དོན་གསལ་བཤད་དང་།ཁྱེད་རང་གིས་གནོན་འོག་གི་མཐེབ་གནོན་སྟེང་དུ་བསྐུར་འདྲ་པར་དང་ཡིག་ཆ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="134"/>
        <source>Remaining</source>
        <translation>དེ་བྱིངས་ཚང་མ་ལྷག</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="135"/>
        <source>character</source>
        <translation>གཤིས་ཀ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="172"/>
        <source>Details</source>
        <translation>འགྲེལ་བཤད་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="184"/>
        <source>ScreenShot</source>
        <translation>བརྙན་ཤེལ་གྱི་པར་རིས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="200"/>
        <source>Add file</source>
        <translation>ཡིག་ཆ་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="258"/>
        <source>The phone number cannot be empty</source>
        <translation>ཁ་པར་ཨང་གྲངས་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="260"/>
        <source>The phone number format is incorrect</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱི་རྣམ་གཞག་ཡང་དག་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="265"/>
        <source>Please enter your phone number</source>
        <translation>ཁྱེད་ཀྱི་ཁ་པར་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>appellation</source>
        <translation>གོང་གཏུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="279"/>
        <source>Contact</source>
        <translation>འབྲེལ་གཏུག་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Email required for anonymous feedback, not required for gitee feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="291"/>
        <source>The mailbox format is incorrect</source>
        <translation>ཡིག་སྒམ་གྱི་རྣམ་གཞག་ཡང་དག་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="297"/>
        <location filename="../UI/uiproblemfeedback.cpp" line="792"/>
        <source>Log in to gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="310"/>
        <source>Mailbox</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="319"/>
        <source>Agree to take mine </source>
        <translation>ངའི་ཁ་ཆད་དང་ལེན་བྱེད་པར་འཐད་ </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="329"/>
        <source>System information</source>
        <translation>མ་ལག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="355"/>
        <source>Submit</source>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="372"/>
        <source>Details type</source>
        <translation>འདེམས་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="388"/>
        <source>Time period</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="423"/>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="426"/>
        <source>lately</source>
        <translation>ཉེ་ཆར།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="427"/>
        <source>days</source>
        <translation>ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="439"/>
        <source>YES</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="449"/>
        <source>NO</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="471"/>
        <source>Upload log</source>
        <translation>ཤིང་ཆ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="477"/>
        <source>Path</source>
        <translation>འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="492"/>
        <source>Export to</source>
        <translation>ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="605"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>ཡིག་ཚགས་5ལས་བརྒལ་མི་ཆོག་པ་དང་། སྤྱིའི་ཤོང་ཚད་10MBལས་མི་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="606"/>
        <source>Supported formats: </source>
        <translation>རྒྱབ་སྐྱོར་གྱི་རྣམ་གཞག་གཤམ་གསལ། </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>Up to 500 characters</source>
        <translation>ཆེས་མང་ན་ཡི་གེ་500ཡོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="237"/>
        <source>Files</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="683"/>
        <source>Repeat addition</source>
        <translation>ཡང་བསྐྱར་ཁ་སྣོན་བྱས་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="699"/>
        <source>Attachment size out of limit</source>
        <translation>ཟུར་གཏོགས་དངོས་པོའི་ཆེ་ཆུང་ཚད་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="809"/>
        <source>Log out of gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="810"/>
        <source>gitee has been associated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="914"/>
        <source>Add attachment</source>
        <translation>ཟུར་གཏོགས་དངོས་པོ་ཁ་སྣོན་</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="92"/>
        <source>Submitted successfully</source>
        <translation>ལེགས་གྲུབ་བྱུང་བའི་སྒོ་ནས་ཕུལ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="96"/>
        <source>Cancel successfully</source>
        <translation>རྒྱལ་ཁ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="100"/>
        <source>System is abnormal, contact technical support</source>
        <translation>རྒྱུད་ཁོངས་རྒྱུན་འགལ་དང་།འབྲེལ་གཏུག་ལག་རྩལ་རྒྱབ་སྐྱོར་རོགས་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>Log and submission is packed, please go</source>
        <translation>ཉིན་ཐོ་དང་འདོན་སྤྲོད་བྱས་ཟིན་ཐུམ་བརྒྱབ་ནས་རོགས་།ཁ་བྲལ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>acquire</source>
        <translation>ཐོབ་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <source>Submission failed</source>
        <translation>ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="107"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>“ཡང་བསྐྱར་ཚོད་ལེན”མནན་ནས་ཡང་བསྐྱར་བརྒྱུད་བསྐུར་བྱེད་པ།ཡང་ན་ཐད་ཀར་ང་ཚོ་དང་འབྲེལ་བ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="129"/>
        <source>Under submission...</source>
        <translation>སྙན་ཞུ་ཕུལ་བའི་འོག་</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="18"/>
        <source>Contact us</source>
        <translation>ང་ཚོ་དང་འབྲེལ་གཏུག་བྱེད</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="39"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="52"/>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="62"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="69"/>
        <source> to get more services</source>
        <translation> དེ་བས་མང་བ་ཐོབ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="86"/>
        <source>Kylin technical services</source>
        <translation>ཅིན་ལིན་ལག་རྩལ་ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="61"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="24"/>
        <source>Feedback</source>
        <translation>ལྡོག་འདྲེན་བསམ་འཆར།</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="25"/>
        <source>Self service</source>
        <translation>རང་གིས་རང་ལ་ཞབས་འདེགས</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="27"/>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
</context>
</TS>
