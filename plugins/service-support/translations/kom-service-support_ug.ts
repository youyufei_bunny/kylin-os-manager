<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>تەپسىلى سەھىپە تاللاش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>سىستېما</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>سىستېما قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>سىستېما قاچىلاش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>سىستېما چۈشۈپ كېتىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>سىستېما ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>كونترول مەركىزى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>سىستېما تەڭشىكى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>سىستېما ئاساسى مەسلىھەتچىلىكى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>سىز يولۇققان مەسىلىلەرنى تەپسىلىي بايان قىلىڭ، مەسىلەن : سىستېمىنى قوزغىتالماسلىق، مۇناسىۋەتلىك لايىھەنى تاپالماسلىق، سىستېما ئىقتىدارى ئېنىق بولماسلىق قاتارلىقلار.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>سىز يولۇققان مەسىلىلەرنى تەپسىلىي بايان قىلىڭ، مەسىلەن : سىرتقى سەپلىمىنى ئۇلاش مەغلۇپ بولۇش، ئورتاق بەھرىلىنىش ئىقتىدارىنى بەلگىلەش، سىرتقى سەپلىمىنى ماسلاشتۇرۇش قاتارلىقلار.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>سىز يولۇققان مەسىلىلەرنى تەپسىلىي تەسۋىرلەڭ، مەسىلەن بوبرا كېيىك يۇمشاق دېتالىنىڭ خاتالىقىغا ئېرىشىش، قاچىلاش ۋە ئۆچۈرۈش.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>مەسىلىڭىزنى تەپسىلىي بايان قىلىڭ، ياكى بۇ يەرگە تەلىپىڭىز ياكى باھايىڭىزنى تولدۇرسىڭىزمۇ بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>پىرولېتارىيات</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>پىرولېتارىيات ماسلىشىش مەسلىھەتچىلىكى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>پىرولېتارىيات شوپۇرى سېتىۋېلىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>ئىشلىتىش ئۈچۈن</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>يۇمشاق دېتال قاچىلاش ۋە قاچىلاش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>يۇمشاق دېتال ئىشلىتىش ۋە خاتالىقنى پاش قىلىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>باشقا</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>پىكىر ۋە تەكلىپ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="149"/>
        <source>Attachment size exceeds limit!</source>
        <translation>قوشۇمچە چوڭلۇقى چەكتىن ئېشىپ كەتتى!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="61"/>
        <source>Failed to create temporary directory!</source>
        <translation>ۋاقىتلىق مۇندەرىجە قۇرۇش مەغلۇپ بولدى!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>ئاتلاش</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>بەت</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>ئومۇمىي</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>بەت</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>ئاتلاش</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>بەت</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="191"/>
        <source>System log</source>
        <translation>سىستېما خاتىرىسى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="192"/>
        <source>Machine</source>
        <translation>ماشىنا</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="193"/>
        <source>Hardware</source>
        <translation>قاتتىق دېتال</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="194"/>
        <source>Drive</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="195"/>
        <source>APP list</source>
        <translation>APP تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="196"/>
        <source>Rules</source>
        <translation>قائىدىلەر</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="197"/>
        <source>Network</source>
        <translation>تور</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="198"/>
        <source>System</source>
        <translation>سىستېما</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>مۇلازىمەت قوللاش</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="81"/>
        <source>ServiceSupport</source>
        <translation>مۇلازىمەت قوللاش</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="84"/>
        <source>Multi-channel technical support services</source>
        <translation>كۆپ خىل يوللار ئارقىلىق تېخنىكا جەھەتتىن ياردەم بېرىش مۇلازىمىتى</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="87"/>
        <source>Feedback</source>
        <translation>پىكىر قالدىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="90"/>
        <source>Self service</source>
        <translation>خالىس مۇلازىمەت</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="103"/>
        <source>website</source>
        <translation>تور بېكىتى</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>History</source>
        <translation>تارىخ - مەدەنىيەت</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Jump to</source>
        <translation>ئاتلاش</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="110"/>
        <source> to get more services</source>
        <translation> تېخىمۇ كۆپ مۇلازىمەتكە ئېرىشىش</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Creation time</source>
        <translation>ئىجادىيەت ۋاقتى</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="71"/>
        <source>bydesign</source>
        <translation>لايىھىلەش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="73"/>
        <source>duplicate</source>
        <translation>كۆپەيتىش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="75"/>
        <source>external</source>
        <translation>سىرتقى</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="77"/>
        <source>fixed</source>
        <translation>مۇقىم</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="79"/>
        <source>notrepro</source>
        <translation>نوچۈپرو</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="81"/>
        <source>postponed</source>
        <translation>كېچىكتۈرۈش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="83"/>
        <source>willnotfix</source>
        <translation>ئەسلىگە كەلتۈرگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="99"/>
        <source>verify</source>
        <translation>دەلىللەش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="102"/>
        <source>Has the issue been resolved?</source>
        <translation>مەسىلە ھەل بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="105"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>مۇبادا جەزىملەشتۈرۈلسە، مەسىلە تاقىلىدۇ، يەنىمۇ ئىلگىرلىگەن ھالدا ھەرىكەت قوللانمايدۇ.</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="107"/>
        <source>resolved</source>
        <translation>ھەل قىلماق</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="109"/>
        <source>cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Description</source>
        <translation>چۈشەندۈرۈش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Progress</source>
        <translation>ئىلگىرىلەش</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="194"/>
        <source>No record</source>
        <translation>خاتىرىسىز</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="195"/>
        <source>There is a network problem, please try again later</source>
        <translation>تور مەسىلىسى بار، سەل تۇرۇپ قايتا سىناپ بېقىڭ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="196"/>
        <source>Loading, please wait</source>
        <translation>قاچىلىنىۋاتىدۇ، ساقلاپ تۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="209"/>
        <source>retry</source>
        <translation>قايتا تىر</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="109"/>
        <source>Advanced</source>
        <translation>ئىلغار</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="114"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>مەسىلىنى تەپسىلىي تەسۋىرلەڭ، سىز تۆۋەندىكى كۇنۇپكىنى بېسىپ رەسىم ياكى ھۆججەت يوللىسىڭىز بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="134"/>
        <source>Remaining</source>
        <translation>قېلىپ قالغانلار</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="135"/>
        <source>character</source>
        <translation>خاراكتېرى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="172"/>
        <source>Details</source>
        <translation>تەپسىلاتى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="184"/>
        <source>ScreenShot</source>
        <translation>كېسىشمە بۆلەك</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="200"/>
        <source>Add file</source>
        <translation>ھۆججەت قوشۇش</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="258"/>
        <source>The phone number cannot be empty</source>
        <translation>تېلېفون نومۇرىنى بوش قويۇشقا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="260"/>
        <source>The phone number format is incorrect</source>
        <translation>تېلېفون نومۇر شەكلى خاتا</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="265"/>
        <source>Please enter your phone number</source>
        <translation>تېلېفون نومۇرىڭىزنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>appellation</source>
        <translation>دىئاسپۇز</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="279"/>
        <source>Contact</source>
        <translation>ئالاقىلاشقۇچى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Email required for anonymous feedback, not required for gitee feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="291"/>
        <source>The mailbox format is incorrect</source>
        <translation>خەت ساندۇقى شەكلى خاتا</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="297"/>
        <location filename="../UI/uiproblemfeedback.cpp" line="792"/>
        <source>Log in to gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="310"/>
        <source>Mailbox</source>
        <translation>خەت ساندۇقى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="319"/>
        <source>Agree to take mine </source>
        <translation>مېنىڭ قولۇمغا ئېلىشقا قوشۇلدى </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="329"/>
        <source>System information</source>
        <translation>سىستېما ئۇچۇرلىرى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="355"/>
        <source>Submit</source>
        <translation>تاپشۇرماق</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="372"/>
        <source>Details type</source>
        <translation>تەپسىلاتى تۈرى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="388"/>
        <source>Time period</source>
        <translation>ۋاقىت مەزگىلى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="423"/>
        <source>Information</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="426"/>
        <source>lately</source>
        <translation>يېقىندىن بۇيان</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="427"/>
        <source>days</source>
        <translation>كۈنلەر</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="439"/>
        <source>YES</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="449"/>
        <source>NO</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="471"/>
        <source>Upload log</source>
        <translation>كۈندىلىك يوللاش خاتىرىسى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="477"/>
        <source>Path</source>
        <translation>يول</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="492"/>
        <source>Export to</source>
        <translation>ئېكسپورت قىلىش</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="605"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>5دىن ئاشمايدىغان ھۆججەت ۋە ئومۇمىي سىغىم 10MB دىن ئېشىپ كەتمەيدۇ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="606"/>
        <source>Supported formats: </source>
        <translation>قوللايدىغان فورماتلار: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>Up to 500 characters</source>
        <translation>ئەڭ كۆپ بولغاندا 500 ھەرپ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="237"/>
        <source>Files</source>
        <translation>قوشۇمچە ھۆججەت</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="683"/>
        <source>Repeat addition</source>
        <translation>قوشۇۋىلاشنى تەكرارلاش</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="699"/>
        <source>Attachment size out of limit</source>
        <translation>چەكتىن ئېشىپ كەتكەن قوشۇمچە چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="809"/>
        <source>Log out of gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="810"/>
        <source>gitee has been associated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="914"/>
        <source>Add attachment</source>
        <translation>قوشۇمچە ھۆججەت قوشۇش</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>قايتا تىر</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="92"/>
        <source>Submitted successfully</source>
        <translation>مۇۋاپىقىيەتلىك يوللانغان ۋاقتى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="96"/>
        <source>Cancel successfully</source>
        <translation>ئەمەلدىن قالدۇرۇش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="100"/>
        <source>System is abnormal, contact technical support</source>
        <translation>سىستېما نورمالسىزلىقى، تېخنىكا جەھەتتىن قوللاش بىلەن ئالاقىلىشىڭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>Log and submission is packed, please go</source>
        <translation>كۈندىلىك خاتىرە ۋە ئوراپ تاپشۇرۇش، كېتىڭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>acquire</source>
        <translation>ئېرىشمەك</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <source>Submission failed</source>
        <translation>يوللاش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="107"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>«قايتا سىناش»نى چېكىپ يەنە بىر قېتىم يوللاڭ، ياكى بىۋاستە بىز بىلەن ئالاقىلىشىڭ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="129"/>
        <source>Under submission...</source>
        <translation>بويسۇنۇش ئاستىدا...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="18"/>
        <source>Contact us</source>
        <translation>بىز بىلەن ئالاقىلىشىڭ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="39"/>
        <source>Mail</source>
        <translation>پوچتا يوللانمىسى</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="52"/>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="62"/>
        <source>website</source>
        <translation>تور بېكىتى</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="69"/>
        <source> to get more services</source>
        <translation> تېخىمۇ كۆپ مۇلازىمەتكە ئېرىشىش</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="86"/>
        <source>Kylin technical services</source>
        <translation>Kylin تېخنىكا مۇلازىمىتى</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="61"/>
        <source>Jump to</source>
        <translation>ئاتلاش</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="24"/>
        <source>Feedback</source>
        <translation>پىكىر قالدىرۋ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="25"/>
        <source>Self service</source>
        <translation>خالىس مۇلازىمەت</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="27"/>
        <source>History</source>
        <translation>تارىخ - مەدەنىيەت</translation>
    </message>
</context>
</TS>
