<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>егжей-тегжейлүү категорияны тандоо</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>Системаны активдештирүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>Системаны орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>Системанын кыйрашы</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>Системалык аткаруу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>Башкаруу борбору</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>Системаны орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>Системалык негиздер боюнча консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>Сураныч, мисалы, силер менен кездешкен проблемаларды майда-чүйдөсүнө чейин айтып: системасын жандандыруу мүмкүн эмес, тиешелүү орнотуулар, система милдеттери так эмес, таба албайт.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>Сураныч, мисалы, сен туш болгон проблемаларды, майда-чүйдөсүнө чейин айтып: Жабдыктар байланыш бербөө, жалпы иш орнотуулары, жабдыктарына жана башка ылайыкташтыруу.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>Сураныч, мисалы, сатып алуу, орнотуу жана Кирин программалык каталарды орнотуудан сыяктуу көйгөйлөр, сен менен жолугушту майда-чүйдөсүнө чейин айтып.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>Сураныч, сиздин суроону майда-чүйдөсүнө чейин айтып, же бул жерде сиздин суроо-жооп же комментарий толтура аласыз.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>Перифериялык</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>Перифериялык ылайыкташтыруу консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>Перифериялык драйвер сатып алуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>Перифериялык колдонуу жана каталар жөнүндө отчет берүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>Тиркеме</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>Программалык камсыздоону орнотуу жана орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>Программалык камсыздоону колдонуу жана каталар жөнүндө отчет берүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>Башка</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>Пикирлер жана сунуштар</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="149"/>
        <source>Attachment size exceeds limit!</source>
        <translation>Тиркеме өлчөмү чектен ашып түшөт!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="61"/>
        <source>Failed to create temporary directory!</source>
        <translation>Убактылуу каталог түзө алган жок!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>Бет</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>бардыгы</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>бет</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>бет</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="191"/>
        <source>System log</source>
        <translation>Системага кирүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="192"/>
        <source>Machine</source>
        <translation>Машина</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="193"/>
        <source>Hardware</source>
        <translation>Аппараттык</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="194"/>
        <source>Drive</source>
        <translation>Диск</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="195"/>
        <source>APP list</source>
        <translation>APP тизмеси</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="196"/>
        <source>Rules</source>
        <translation>Эрежелер</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="197"/>
        <source>Network</source>
        <translation>Тармак</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="198"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>utumiki</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="81"/>
        <source>ServiceSupport</source>
        <translation>utumiki</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="84"/>
        <source>Multi-channel technical support services</source>
        <translation>Көп-канал техникалык колдоо кызматтары</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="87"/>
        <source>Feedback</source>
        <translation>Пикирлер</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="90"/>
        <source>Self service</source>
        <translation>Өзүмчүл кызмат</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="103"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>History</source>
        <translation>Тарых</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="110"/>
        <source> to get more services</source>
        <translation> көбүрөөк кызматтарды алуу үчүн</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Creation time</source>
        <translation>Жаратуу убактысы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="71"/>
        <source>bydesign</source>
        <translation>Malingana ndi kapangidwe</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="73"/>
        <source>duplicate</source>
        <translation>копиялоо</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="75"/>
        <source>external</source>
        <translation>Сырткы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="77"/>
        <source>fixed</source>
        <translation>Негизги-белгиленген</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="79"/>
        <source>notrepro</source>
        <translation>Novo жалпы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="81"/>
        <source>postponed</source>
        <translation>Kuwonjezeredwa</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="83"/>
        <source>willnotfix</source>
        <translation>Simungathe kukonzedwa</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="99"/>
        <source>verify</source>
        <translation>Onetsetsani</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="102"/>
        <source>Has the issue been resolved?</source>
        <translation>Kodi vutoli lasinthidwa?</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="105"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>Бир жолу аныкталат, маселе жабылып калат, андан ары иш-аракет талап кылбайт.</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="107"/>
        <source>resolved</source>
        <translation>Solution</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="109"/>
        <source>cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Description</source>
        <translation>сүрөттөлүшү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Progress</source>
        <translation>прогресс</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="194"/>
        <source>No record</source>
        <translation>Palibe zolemba</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="195"/>
        <source>There is a network problem, please try again later</source>
        <translation>тармак көйгөйү бар, сураныч, кийин кайра аракет</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="196"/>
        <source>Loading, please wait</source>
        <translation>жүктөө, сураныч, күтүү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="209"/>
        <source>retry</source>
        <translation>ретри</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="109"/>
        <source>Advanced</source>
        <translation>Өнүккөн</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="114"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>Сураныч, майда-чүйдөсүнө чейин маселени түшүндүрүп, сиз сүрөттөрдү же материалдары жүктөп төмөнкү баскычты басып алат.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="134"/>
        <source>Remaining</source>
        <translation>Калган</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="135"/>
        <source>character</source>
        <translation>мүнөзү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="172"/>
        <source>Details</source>
        <translation>Майда-чүйдөсүнө</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="184"/>
        <source>ScreenShot</source>
        <translation>ScreenShot</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="200"/>
        <source>Add file</source>
        <translation>Файлды кошуу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="258"/>
        <source>The phone number cannot be empty</source>
        <translation>Телефон номери бош болушу мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="260"/>
        <source>The phone number format is incorrect</source>
        <translation>Телефон номеринин форматы туура эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="265"/>
        <source>Please enter your phone number</source>
        <translation>Телефон номериңизди киргизиңиз</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>appellation</source>
        <translation>аппеляция</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="279"/>
        <source>Contact</source>
        <translation>Байланыш</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Email required for anonymous feedback, not required for gitee feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="291"/>
        <source>The mailbox format is incorrect</source>
        <translation>Почта кутучасынын форматы туура эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="297"/>
        <location filename="../UI/uiproblemfeedback.cpp" line="792"/>
        <source>Log in to gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="310"/>
        <source>Mailbox</source>
        <translation>Почта кутучасы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="319"/>
        <source>Agree to take mine </source>
        <translation>Мени алып кетүүгө макул болгула </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="329"/>
        <source>System information</source>
        <translation>Система тууралуу маалымат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="355"/>
        <source>Submit</source>
        <translation>Тапшыруу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="372"/>
        <source>Details type</source>
        <translation>Маалымат түрү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="388"/>
        <source>Time period</source>
        <translation>Убакыт мезгили</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="423"/>
        <source>Information</source>
        <translation>Маалымат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="426"/>
        <source>lately</source>
        <translation>акыркы күндөрдө</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="427"/>
        <source>days</source>
        <translation>күн</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="439"/>
        <source>YES</source>
        <translation>ООБА</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="449"/>
        <source>NO</source>
        <translation>ЖОК</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="471"/>
        <source>Upload log</source>
        <translation>Жүктөө журналы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="477"/>
        <source>Path</source>
        <translation>Жол</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="492"/>
        <source>Export to</source>
        <translation>Экспорттоо</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="605"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>5 файлдан ашык эмес жана жалпы кубаттуулугу 10МБ ашык эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="606"/>
        <source>Supported formats: </source>
        <translation>Колдоого алынган форматтар: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>Up to 500 characters</source>
        <translation>500 символго чейин</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="237"/>
        <source>Files</source>
        <translation>Тиркемелер</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="683"/>
        <source>Repeat addition</source>
        <translation>Кошумчаны кайталоо</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="699"/>
        <source>Attachment size out of limit</source>
        <translation>Тиркеме өлчөмү чектен чыккан</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="809"/>
        <source>Log out of gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="810"/>
        <source>gitee has been associated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="914"/>
        <source>Add attachment</source>
        <translation>Тиркемени кошуу</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>МАКУЛ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>ретри</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="92"/>
        <source>Submitted successfully</source>
        <translation>Ийгиликтүү жөнөтүлдү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="96"/>
        <source>Cancel successfully</source>
        <translation>ийгиликтүү жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="100"/>
        <source>System is abnormal, contact technical support</source>
        <translation>Ndondomeko yachilendo, chonde tanani ndi chithandizo chamakono</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>Log and submission is packed, please go</source>
        <translation>Lembani ndi kutumiza zakonzedwa, chonde musiye</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>acquire</source>
        <translation>Pezani</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <source>Submission failed</source>
        <translation>Сунуштоо ишке ашпады</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="107"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>&quot;Кайра аракет&quot; кайра жүктөп, же түздөн-түз бизге кайрылып.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="129"/>
        <source>Under submission...</source>
        <translation>Сунуштоо боюнча...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="18"/>
        <source>Contact us</source>
        <translation>Биз менен байланыш</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="39"/>
        <source>Mail</source>
        <translation>Почта</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="52"/>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="62"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="69"/>
        <source> to get more services</source>
        <translation> көбүрөөк кызматтарды алуу үчүн</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="86"/>
        <source>Kylin technical services</source>
        <translation>Кайлин техникалык кызматтары</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="61"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="24"/>
        <source>Feedback</source>
        <translation>Пикирлер</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="25"/>
        <source>Self service</source>
        <translation>Өзүмчүл кызмат</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="27"/>
        <source>History</source>
        <translation>Тарых</translation>
    </message>
</context>
</TS>
