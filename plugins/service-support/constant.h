#ifndef CONSTANT_H
#define CONSTANT_H

#define NAME_HISTORY_FEEDBACK "HistoryFeedback"
#define NAME_PROBLEM_FEEDBACK "ProblemFeedback"
#define NAME_SELF_SERVICE "SelfService"

enum InternalMode { OpenKylin = 3 };

#endif // CONSTANT_H
