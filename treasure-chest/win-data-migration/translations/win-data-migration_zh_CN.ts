<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QApplication</name>
    <message>
        <source>Win Data Migration</source>
        <translation type="unfinished">win迁移工具</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Win Data Migration</source>
        <translation>win迁移工具</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>未成功迁移的文件</translation>
    </message>
    <message>
        <source>view</source>
        <translation>查看</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>读取迁移日志失败：</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>迁移日志</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>win端如何设置</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>win端如何设置（必读）</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>win工作组</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>win用户名</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>win密码</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>建立连接</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>连接成功</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>正在连接中...</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>连接失败，请重试</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>选择要迁移的文件</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>文件迁移至</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>开始迁移</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>选择迁移目录</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>迁移完成</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>创建迁移日志失败</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>个文件迁移失败</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>未选中任何迁移项</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>保存路径不合法或无权限</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>创建文件失败</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>文件完整性校验不通过</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>【加载中】</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>smb连接异常中断</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>读取迁移文件树...</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;win端设置 &lt;/h2&gt;&lt;p&gt;第一步：两台电脑需在同一局域网网络内&lt;/p&gt;&lt;p&gt; 第二步：win电脑需将文件夹或文件，设置为共享文件夹 &lt;/p&gt;&lt;div&gt;1、选择要共享的文件夹。&lt;/div&gt;&lt;div&gt;2、鼠标右键点击属性，并选择共享页签，点击高级共享设置，勾选共享此文件夹，点击应用。&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>正在取消文件迁移，请稍候……</translation>
    </message>
</context>
</TS>
